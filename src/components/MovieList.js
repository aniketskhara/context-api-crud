import React, { useState, useContext } from 'react';
import { MovieContext } from './MovieContext';
import Movie from './Movie';

const MovieList = () => {

    // set context value in some variable
    const [movies, setMovies] = useContext(MovieContext);

    return (
        <div>
            <h1>{movies.map(movie => (
                <Movie name={movie.name} price={movie.price} />
            ))}</h1>
        </div>
    );
}

export default MovieList;