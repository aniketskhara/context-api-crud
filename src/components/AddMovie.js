import React, { useState, useContext } from 'react';

// To use context api
import { MovieContext } from './MovieContext';

const AddMovie = () => {

    const [movies, setMovies] = useContext(MovieContext);

    const [name, setName] = useState('');
    const [price, setPrice] = useState('');

    const updateName = (e) => {
        setName(e.target.value)
    }

    const updatePrice = (e) => {
        setPrice(e.target.value)
    }

    const addMovie = (e) => {
        e.preventDefault();
        setMovies(prev => [...prev, { name: name, price: price}])
        setName('');
        setPrice('');
    }

    return (
        <div>
            <form onSubmit={addMovie}>
                <input type="text" name="name" value={name} onChange={updateName}/>
                <input type="text" name="price" value={price} onChange={updatePrice}/>
                < button>Add Movie</button>
            </form>
        </div>
    );
}

export default AddMovie;