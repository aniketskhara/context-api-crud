import React, { useState, useContext } from 'react';
// To use context api
import { MovieContext } from './MovieContext';

const Nav = () => {

    //fetch value from context api
    const [movies, setMovies] = useContext(MovieContext);

    return (
        <div>
            <div>Hello Aniket</div>
            <p>List of movies: {movies.length}</p>
        </div>
    );
}

export default Nav;