import React from 'react';


const Movie = ({ name, price }) => {
    return (
        <div>
            <li>
                <h6>{name}</h6>
                <p>{price}</p>
            </li>
        </div>
    );
}

export default Movie;