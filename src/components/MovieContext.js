import React, { useState, createContext } from 'react';

// import this in file where you want to use context api
export const MovieContext = createContext();

// import this in app.js
export const MovieProvider = (props) => {
    const [movies, setMovies] = useState([
        {
            name: 'Game of Thrones',
            price: '$10'
        },
        {
            name: 'Inception',
            price: '$15'
        },
        {
            name: 'Parasite',
            price: '$20'
        }
    ]);

    return (
        //sending value
        <MovieContext.Provider value={[movies, setMovies]}>
            {props.children}
        </MovieContext.Provider>
    );
}
